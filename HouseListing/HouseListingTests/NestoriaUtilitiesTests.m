//
//  NestoriaUtilitiesTests.m
//  HouseListingTests
//
//  Created by Benjamin Rosen on 02/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NestoriaUtilities.h"
#import "HouseListingModel.h"

@interface NestoriaUtilitiesTests : XCTestCase

@end

@implementation NestoriaUtilitiesTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testValidNestoriaUrlReturnedWithoutAdditionalQueryItems {
    NSURL *testURL = [NestoriaUtilities listingsRequestUrlOnPage:nil maxBedrooms:nil maxPrice:nil];
    NSString *expectedUrl = @"https://api.nestoria.co.uk/api?encoding=json&action=search_listings&listing_type=buy&place_name=bristol";
    XCTAssertTrue([expectedUrl isEqualToString:[testURL absoluteString]]);
}

- (void)testValidNestoriaUrlReturnedWithAdditionalQueryItems {
    NSURL *testURL = [NestoriaUtilities listingsRequestUrlOnPage:@(2) maxBedrooms:@(3) maxPrice:@(250000)];
    NSString *expectedUrl = @"https://api.nestoria.co.uk/api?encoding=json&action=search_listings&listing_type=buy&place_name=bristol&page=2&bedroom_max=3&price_max=250000";
    XCTAssertTrue([expectedUrl isEqualToString:[testURL absoluteString]]);
}

- (void)testInvalidJsonDataReturnsNoListings {

    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"invalidNestoriaListingsExample" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:path];
    NSArray<HouseListingModel *> *listings = [NestoriaUtilities houseListingsFromData:jsonData];
    XCTAssertNil(listings);
}

- (void)testValidJsonDataReturnsListings {
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"nestoriaListingsExample" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:path];
    NSArray<HouseListingModel *> *listings = [NestoriaUtilities houseListingsFromData:jsonData];
    XCTAssertNotNil(listings);
    XCTAssertEqual(2, listings.count);
    XCTAssertTrue([@"Cornwall Gardens, Brighton, BN1" isEqualToString:listings[0].title]);
    XCTAssertTrue([@"Sitting in one of the citys most distinctive and prestigious enclav..."
                   isEqualToString:listings[0].summary]);
    XCTAssertTrue([@"https://imgs.nestimg.com/medium/6_bedroom_semi_detached_house_for_sale_110629430336473931.jpg"
                   isEqualToString:listings[0].thumbnailLocation]);
    XCTAssertTrue([@"https://imgs.nestimg.com/6_bedroom_semi_detached_house_for_sale_110629430336473931.jpg"
                   isEqualToString:listings[0].imageLocation]);
    XCTAssertTrue([@"OnTheMarket.com" isEqualToString:listings[0].dataSourceName]);
    XCTAssertTrue([@"£1,100,000" isEqualToString:listings[0].formattedPrice]);
    XCTAssertEqual(1100000, [listings[0].price integerValue]);

    XCTAssertTrue([@"Hythe Road, Fiveways, Brighton" isEqualToString:listings[1].title]);
    XCTAssertTrue([@"Last house remaininga discrete new development of 4 bedroom townhou..."
                   isEqualToString:listings[1].summary]);
    XCTAssertTrue([@"https://imgs.nestimg.com/medium/5_bedroom_terraced_house_for_sale_111624985548097051.jpg"
                   isEqualToString:listings[1].thumbnailLocation]);
    XCTAssertTrue([@"https://imgs.nestimg.com/5_bedroom_terraced_house_for_sale_111624985548097051.jpg"
                   isEqualToString:listings[1].imageLocation]);
    XCTAssertTrue([@"Zoopla.co.uk" isEqualToString:listings[1].dataSourceName]);
    XCTAssertTrue([@"£695,000" isEqualToString:listings[1].formattedPrice]);
    XCTAssertEqual(695000, [listings[1].price integerValue]);

}

@end
