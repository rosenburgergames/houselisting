//
//  HouseListingModelTests.m
//  HouseListingTests
//
//  Created by Benjamin Rosen on 02/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HouseListingModel.h"

static NSString * const TEST_TITLE = @"testTitle";
static NSString * const TEST_SUMMARY = @"testSummary";
static NSString * const TEST_THUMBNAIL_LOCATION = @"thumbLocation.png";
static NSString * const TEST_IMAGE_LOCATION = @"imageLocation.png";
static NSString * const DATA_SOURCE_NAME = @"dataSource";
static NSInteger const TEST_PRICE = 123;
static NSString * const TEST_PRICE_FORMATTED = @"£123";

@interface HouseListingModelTests : XCTestCase

@end

@implementation HouseListingModelTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testHouseListingModelConstructedFromValidDictionary {
    NSDictionary *dictionary = @{ @"title" : TEST_TITLE,
                                  @"summary" : TEST_SUMMARY,
                                  @"thumb_url" : TEST_THUMBNAIL_LOCATION,
                                  @"img_url" : TEST_IMAGE_LOCATION,
                                  @"datasource_name" : DATA_SOURCE_NAME,
                                  @"price" : @(TEST_PRICE),
                                  @"price_formatted" : TEST_PRICE_FORMATTED};


    HouseListingModel *model = [HouseListingModel fromDictionary:dictionary];

    XCTAssertNotNil(model);
    XCTAssertTrue([TEST_TITLE isEqualToString: model.title]);
    XCTAssertTrue([TEST_SUMMARY isEqualToString:model.summary]);
    XCTAssertTrue([TEST_THUMBNAIL_LOCATION isEqualToString:model.thumbnailLocation]);
    XCTAssertTrue([TEST_IMAGE_LOCATION isEqualToString:model.imageLocation]);
    XCTAssertEqual(TEST_PRICE, [model.price integerValue]);
    XCTAssertTrue([TEST_PRICE_FORMATTED isEqualToString:model.formattedPrice]);
}


- (void)testHouseListingModelNilWithDataMissing {
    NSDictionary *dictionary = @{ @"title" : TEST_TITLE,
                                  @"summary" : TEST_SUMMARY,
                                  @"thumb_url" : TEST_THUMBNAIL_LOCATION,
                                  @"img_url" : TEST_IMAGE_LOCATION,
                                  @"datasource_name" : DATA_SOURCE_NAME,
                                  @"price_formatted" : TEST_PRICE_FORMATTED};


    HouseListingModel *model = [HouseListingModel fromDictionary:dictionary];

    XCTAssertNil(model);
}

@end
