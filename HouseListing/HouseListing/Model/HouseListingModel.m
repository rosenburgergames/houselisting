//
//  HouseListingModel.m
//  HouseListing
//
//  Created by Benjamin Rosen on 02/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import "HouseListingModel.h"

@implementation HouseListingModel

- (instancetype) initWithTitle:(NSString *)title summary:(NSString *)summary
                    dataSource:(NSString *)dataSourceName
             thumbnailLocation:(NSString *)thumbnailLocation
                 imageLocation:(NSString *)imageLocation
                         price:(NSNumber *)price
                formattedPrice:(NSString *)formattedPrice {
    self = [super init];
    if (self) {
        _title = title;
        _summary = summary;
        _dataSourceName = dataSourceName;
        _thumbnailLocation = thumbnailLocation;
        _imageLocation = imageLocation;
        _price = price;
        _formattedPrice = formattedPrice;
    }
    return self;
}

+ (HouseListingModel *)fromDictionary:(NSDictionary *)dictionary
{
    NSString *title = dictionary[@"title"];
    NSString *summary = dictionary[@"summary"];
    NSString *dataSource = dictionary[@"datasource_name"];
    NSString *thumbnailLocation = dictionary[@"thumb_url"];
    NSString *imageLocation= dictionary[@"img_url"];
    NSNumber *price = dictionary[@"price"];
    NSString *formattedPrice = dictionary[@"price_formatted"];

    if (title && summary && dataSource && thumbnailLocation
        && imageLocation && price && formattedPrice) {
        return [[HouseListingModel alloc] initWithTitle:title summary:summary
                                        dataSource:dataSource
                                 thumbnailLocation:thumbnailLocation
                                     imageLocation:imageLocation
                                             price:price
                                    formattedPrice:formattedPrice];
    }

    return nil;
}

@end
