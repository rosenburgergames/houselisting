//
//  HouseListingModel.h
//  HouseListing
//
//  Created by Benjamin Rosen on 02/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HouseListingModel : NSObject

@property (strong, nonatomic, readonly) NSString *title;
@property (strong, nonatomic, readonly) NSString *summary;
@property (strong, nonatomic, readonly) NSString *thumbnailLocation;
@property (strong, nonatomic, readonly) NSString *imageLocation;
@property (strong, nonatomic, readonly) NSString *dataSourceName;
@property (strong, nonatomic, readonly) NSNumber *price;
@property (strong, nonatomic, readonly) NSString *formattedPrice;

+ (HouseListingModel *)fromDictionary:(NSDictionary *)dictionary;

@end
