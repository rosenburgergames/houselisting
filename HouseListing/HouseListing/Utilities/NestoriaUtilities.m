//
//  NestoriaUtilities.m
//  HouseListing
//
//  Created by Benjamin Rosen on 02/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import "NestoriaUtilities.h"
#import "HouseListingModel.h"

@implementation NestoriaUtilities

+ (NSURL *)listingsRequestUrlOnPage:(NSNumber *)page maxBedrooms:(NSNumber *)maxBedrooms maxPrice:(NSNumber *)maxPrice
{
    NSURLComponents *components = [[NSURLComponents alloc] initWithString:@"https://api.nestoria.co.uk/api"];
    NSURLQueryItem *encoding = [[NSURLQueryItem alloc] initWithName:@"encoding" value:@"json"];
    NSURLQueryItem *action = [[NSURLQueryItem alloc] initWithName:@"action" value:@"search_listings"];
    NSURLQueryItem *listingType = [[NSURLQueryItem alloc] initWithName:@"listing_type" value:@"buy"];
    NSURLQueryItem *placeName = [[NSURLQueryItem alloc] initWithName:@"place_name" value:@"bristol"];

    NSMutableArray<NSURLQueryItem *> *queryItems = [@[encoding, action, listingType, placeName] mutableCopy];

    if (page) {
        NSURLQueryItem *pageQueryItem = [[NSURLQueryItem alloc] initWithName:@"page" value:[page stringValue]];
        [queryItems addObject:pageQueryItem];
    }

    if (maxBedrooms) {
        NSURLQueryItem *bedroomQueryItem = [[NSURLQueryItem alloc] initWithName:@"bedroom_max" value:[maxBedrooms stringValue]];
        [queryItems addObject: bedroomQueryItem];
    }

    if (maxPrice) {
        NSURLQueryItem *maxPriceQueryItem = [[NSURLQueryItem alloc] initWithName:@"price_max" value:[maxPrice stringValue]];
        [queryItems addObject: maxPriceQueryItem];
    }

    [components setQueryItems:queryItems];

    return components.URL;
}

+ (NSArray<HouseListingModel *> *)houseListingsFromData:(NSData *)data
{
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

    if (error) {
        NSLog(@"Error occured when serializing house listing data: %@", error.localizedDescription);
        return nil;
    }

    NSArray *listingsData = json[@"response"][@"listings"];

    if (!listingsData) {
        NSLog(@"Could not find listings in JSON response");
        return nil;
    }

    NSMutableArray<HouseListingModel *> *listings = [[NSMutableArray alloc] init];

    for (NSDictionary *listingData in listingsData) {
        HouseListingModel *listing = [HouseListingModel fromDictionary:listingData];

        if (listing) {
            [listings addObject:listing];
        } else {
            NSLog(@"Could not convert listing to model object");
        }
    }

    return listings;
}

@end
