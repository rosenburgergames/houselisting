//
//  NestoriaUtilities.h
//  HouseListing
//
//  Created by Benjamin Rosen on 02/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HouseListingModel;

@interface NestoriaUtilities : NSObject

+ (NSURL *)listingsRequestUrlOnPage:(NSNumber *)page maxBedrooms:(NSNumber *)maxBedrooms maxPrice:(NSNumber *)maxPrice;

+ (NSArray<HouseListingModel *> *)houseListingsFromData:(NSData *)data;

@end

