//
//  FiltersViewController.h
//  HouseListing
//
//  Created by Benjamin Rosen on 03/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FiltersViewControllerDelegate <NSObject>

- (void)filtersClosed;
- (void)filtersAppliedWithMaxBedrooms:(NSNumber *)maxBedrooms maxPrice:(NSNumber *)maxPrice;

@end

@interface FiltersViewController : UIViewController

@property (weak, nonatomic) id<FiltersViewControllerDelegate> delegate;

- (void)setMaxPrice:(NSNumber *)maxPrice;
- (void)setMaxBedrooms:(NSNumber *)maxBedrooms;

@end
