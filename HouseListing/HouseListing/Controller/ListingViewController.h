//
//  ListingViewController.h
//  HouseListing
//
//  Created by Benjamin Rosen on 03/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HouseListingModel;

@interface ListingViewController : UIViewController

@property (strong, nonatomic) HouseListingModel *listing;

@end

NS_ASSUME_NONNULL_END
