//
//  AppDelegate.h
//  HouseListing
//
//  Created by Benjamin Rosen on 02/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

