//
//  FiltersViewController.m
//  HouseListing
//
//  Created by Benjamin Rosen on 03/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import "FiltersViewController.h"

@interface FiltersViewController ()
@property (strong, nonatomic) IBOutlet UISegmentedControl *maxBedroomsControl;
@property (strong, nonatomic) IBOutlet UISlider *maxPriceSlider;
@property (strong, nonatomic) IBOutlet UILabel *maxPriceLabel;

@property (strong, nonatomic) NSNumber *initialMaxBedrooms;
@property (strong, nonatomic) NSNumber *initialMaxPrice;

@end

@implementation FiltersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupMaxBedroomsControl];
    [self setupMaxPriceSlider];
}

- (void)setupMaxBedroomsControl
{
    if (!self.initialMaxBedrooms ||
        self.initialMaxBedrooms.intValue >= self.maxBedroomsControl.numberOfSegments) {
        self.maxBedroomsControl.selectedSegmentIndex = 0;
    } else {
        self.maxBedroomsControl.selectedSegmentIndex = self.initialMaxBedrooms.integerValue;
    }
}

- (void)setupMaxPriceSlider
{
    if (self.initialMaxPrice) {
        [self.maxPriceSlider setValue:[self.initialMaxPrice floatValue]];
    } else {
        [self.maxPriceSlider setValue:self.maxPriceSlider.maximumValue];
    }
    self.maxPriceLabel.text = [FiltersViewController getPriceSliderText:self.maxPriceSlider];
}

- (IBAction)sliderValueChanged:(UISlider *)sender
{
    self.maxPriceLabel.text = [FiltersViewController getPriceSliderText:sender];
}

- (void)setMaxBedrooms:(NSNumber *)maxBedrooms
{
    _initialMaxBedrooms = maxBedrooms;
}

- (void)setMaxPrice:(NSNumber *)maxPrice
{
    _initialMaxPrice = maxPrice;
}

- (NSNumber *)getMaxBedrooms
{
    NSInteger segmentIndex = self.maxBedroomsControl.selectedSegmentIndex;

    if (segmentIndex == 0) {
        return nil;
    }

    return [NSNumber numberWithInteger:segmentIndex];
}

- (NSNumber *)getMaxPrice
{
    float sliderValue = self.maxPriceSlider.value;

    if (self.maxPriceSlider.value >= self.maxPriceSlider.maximumValue) {
        return nil;
    }

    return [NSNumber numberWithInt:(int)sliderValue];
}

- (IBAction)applyFiltersPressed:(id)sender
{
    [self.delegate filtersAppliedWithMaxBedrooms:[self getMaxBedrooms] maxPrice:[self getMaxPrice]];
}

- (IBAction)closeFiltersPressed:(id)sender {
    [self.delegate filtersClosed];
}

+ (NSString *)getPriceSliderText:(UISlider *)slider
{
    NSNumber *value = [NSNumber numberWithInt:(int)slider.value];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    formatter.currencyCode = @"GBP";
    [formatter setMaximumFractionDigits:0];
    NSString *valueText = [formatter stringFromNumber:value];
    if (slider.value >= slider.maximumValue) {
        valueText = [valueText stringByAppendingString:@"+"];
    }
    return valueText;
}

@end
