//
//  ListingsTableViewController.m
//  HouseListing
//
//  Created by Benjamin Rosen on 03/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import "ListingsTableViewController.h"
#import "ListingTableViewCell.h"
#import "HouseListingModel.h"
#import <SDWebImage/SDWebImage.h>

@interface ListingsTableViewController ()

@property (strong, nonatomic) NSArray<HouseListingModel *> *listings;

@end

@implementation ListingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 200;
}

- (void)setListings:(NSArray<HouseListingModel *> *)listings
{
    _listings = listings;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.listings ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ListingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListingCell" forIndexPath:indexPath];
    HouseListingModel *listing = self.listings[indexPath.row];
    NSURL *imageUrl = [NSURL URLWithString:listing.thumbnailLocation];
    [cell.listingImageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"PlaceholderImage"]];
    cell.titleLabel.text = listing.title;
    cell.summaryLabel.text = listing.summary;
    cell.priceLabel.text = listing.formattedPrice;
    [cell layoutIfNeeded];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HouseListingModel *selectedListing = self.listings[indexPath.row];
    [self.delegate listingsTableViewControllerDidSelectListing:selectedListing];
}

#pragma mark - ViewControllerDelegate

- (void)viewControllerDidFinishLoadingWithListings:(NSArray<HouseListingModel *> *)listings
{
    self.listings = listings;

}



@end
