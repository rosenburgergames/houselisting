//
//  ListingsTableViewController.h
//  HouseListing
//
//  Created by Benjamin Rosen on 03/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ListingTableViewControllerDelegate <NSObject>

- (void)listingsTableViewControllerDidSelectListing:(HouseListingModel *)listing;

@end

@interface ListingsTableViewController : UITableViewController

@property (nonatomic, weak) id<ListingTableViewControllerDelegate> delegate;

- (void)setListings:(NSArray<HouseListingModel *> *)listings;

@end

NS_ASSUME_NONNULL_END
