//
//  ViewController.m
//  HouseListing
//
//  Created by Benjamin Rosen on 02/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import "ViewController.h"
#import "NestoriaUtilities.h"
#import "ListingsTableViewController.h"
#import "ListingViewController.h"
#import "ListingsTableViewController.h"
#import "FiltersViewController.h"

enum LoadingStatus {
    Loading,
    Complete,
    Failed
};

typedef enum LoadingStatus LoadingStatus;

@interface ViewController () <ListingTableViewControllerDelegate, FiltersViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *retryButton;
@property (strong, nonatomic) IBOutlet UILabel *downloadFailureLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (strong, nonatomic) IBOutlet UIView *tableViewContainer;

@property (strong, nonatomic) NSNumber *page;
@property (strong, nonatomic) NSNumber *maxBedrooms;
@property (strong, nonatomic) NSNumber *maxPrice;

@property (strong, nonatomic) ListingsTableViewController *listingsTableViewController;

@end

@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EmbeddedTableViewController"]) {
        self.listingsTableViewController = segue.destinationViewController;
        self.listingsTableViewController.delegate = self;
    } else if ([segue.identifier isEqualToString:@"FiltersViewController"])  {
        FiltersViewController *vc = segue.destinationViewController;
        [vc setMaxPrice:self.maxPrice];
        [vc setMaxBedrooms:self.maxBedrooms];
        vc.delegate = self;

    }
}

- (void)loadData
{
    [self toggleLoadingViews:Loading];
    NSURL *dataUrl = [NestoriaUtilities listingsRequestUrlOnPage:self.page maxBedrooms:self.maxBedrooms maxPrice:self.maxPrice];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithURL:dataUrl completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {

        if (error) {
            NSLog(@"Error loading data: %@", error.localizedDescription);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self toggleLoadingViews:Failed];
            });
            return;
        }

        NSArray *listings = [NestoriaUtilities houseListingsFromData:data];

        if (!listings) {
            NSLog(@"Could not get listings from data");
            dispatch_async(dispatch_get_main_queue(), ^{
                [self toggleLoadingViews:Failed];
            });
            return;
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            [self.listingsTableViewController setListings:listings];
            [self toggleLoadingViews:Complete];
        });
    }];
    [task resume];
}


- (void)toggleLoadingViews:(LoadingStatus)loadingStatus
{
    switch (loadingStatus) {
        case Loading:
            [self.loadingIndicator setHidden:NO];
            [self.loadingIndicator startAnimating];
            [self setRetryViewsHidden:YES];
            [self.tableViewContainer setHidden:YES];
            break;
        case Complete:
            [self.loadingIndicator stopAnimating];
            [self.loadingIndicator setHidden:YES];
            [self setRetryViewsHidden:YES];
            [self.tableViewContainer setHidden:NO];

            break;
        case Failed:
            [self.loadingIndicator stopAnimating];
            [self.loadingIndicator setHidden:YES];
            [self setRetryViewsHidden:NO];
            [self.tableViewContainer setHidden:YES];
            break;
    }

}

- (void)setRetryViewsHidden:(BOOL)areHidden
{
    [self.retryButton setHidden:areHidden];
    [self.downloadFailureLabel setHidden:areHidden];
}

- (IBAction)retryDataLoad:(id)sender
{
    [self loadData];
}

- (void)listingsTableViewControllerDidSelectListing:(nonnull HouseListingModel *)listing {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ListingViewController *listingViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ListingViewController"];
    [listingViewController setListing:listing];
    [self showViewController:listingViewController sender:self];
}


- (void)filtersAppliedWithMaxBedrooms:(NSNumber *)maxBedrooms maxPrice:(NSNumber *)maxPrice {
    self.maxBedrooms = maxBedrooms;
    self.maxPrice = maxPrice;
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        [self loadData];
    }];
}

- (void)filtersClosed
{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
