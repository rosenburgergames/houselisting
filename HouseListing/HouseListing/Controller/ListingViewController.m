//
//  ListingViewController.m
//  HouseListing
//
//  Created by Benjamin Rosen on 03/06/2019.
//  Copyright © 2019 Benjamin Rosen. All rights reserved.
//

#import "ListingViewController.h"
#import <SDWebImage/SDWebImage.h>
#import "HouseListingModel.h"

@interface ListingViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *listingImageView;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *summaryLabel;
@property (strong, nonatomic) IBOutlet UILabel *dataSourceLabel;

@end

@implementation ListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.listing.title;
    NSURL *imageUrl = [NSURL URLWithString:self.listing.imageLocation];
    [self.listingImageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"PlaceholderImage"]];
     [self.priceLabel setText:self.listing.formattedPrice];
     [self.summaryLabel setText:self.listing.summary];

     NSString *dataSourceFormat = NSLocalizedStringFromTable(@"Advertised by: %@", @"HouseListing",
                                                             @"Format for showing the data source");
     NSString *dataSourceText = [NSString stringWithFormat:dataSourceFormat, self.listing.dataSourceName];
     [self.dataSourceLabel setText:dataSourceText];
}

@end
