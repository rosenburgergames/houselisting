# House Listing App

## Features

- This app presents house sale listings for Bristol using the Nestoria API.
- Listings can be filtered by max price/bedrooms.
- Clicking on a listing shows further details.
- If the data cannot be downloaded the user is prompted to check their internet connection and can retry.

## Build and maintenance

- The app uses one third party library: SDWebImage, for loading online images to views.
- This library was installed using CocoaPods, the installed files should be included, if not run `pod install` in the folder containing the Podfile.
- When opening with Xcode you must use the 'xcworkspace' file to be able to build the app.
- There are some unit tests which can be run from Xcode.
